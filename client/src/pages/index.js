import Appointment from './Appointments/Appointments';
import Billing from './Billing/Billing';
import Home from './Home/Home';
import LogIn from './LogIn/LogIn';
import Register from './Register/Register';

export {
    Appointment,
    Billing,
    Home,
    LogIn,
    Register
}